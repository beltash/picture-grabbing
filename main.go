package main

import (
	"encoding/base64"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"runtime"
	"sync"
)

var base64Encoded = make(map[int]template.URL) //map with valid image paths
var mutex = &sync.Mutex{}
var total int = 0 //image's counter

func encode(bin []byte) []byte {
	e64 := base64.StdEncoding
	maxEncLen := e64.EncodedLen(len(bin))
	encBuf := make([]byte, maxEncLen)
	e64.Encode(encBuf, bin)
	return encBuf
}

func format(enc []byte, mime string) string {
	switch mime {
	case "image/gif", "image/jpeg", "image/pjpeg", "image/png", "image/tiff":
		return fmt.Sprintf("data:%s;base64,%s", mime, enc)
	default:
	}
	return fmt.Sprintf("data:image/png;base64,%s", enc)
}

func saveImg(w http.ResponseWriter, pathImg string, url string, i int) {

	var curUrl string = ""
	var imageUrl string = ""
	var hostname string = url

	//make valid image path - imgeUrl
	isAbsolute, err := regexp.MatchString(`^/[^/]`, pathImg)
	withoutProt, err := regexp.MatchString(`^//`, pathImg)
	isFull, err := regexp.MatchString(`^https?:`, pathImg)
	if isAbsolute {
		re := regexp.MustCompile(`^https?://[^/]+`)
		hostname = re.FindString(url)
		imageUrl = hostname + pathImg
	} else if isFull {
		imageUrl = pathImg
	} else if withoutProt {
		res := regexp.MustCompile(`^https?:`)
		curUrl = res.FindString(url)
		imageUrl = curUrl + pathImg
	} else {
		res := regexp.MustCompile(`^https?://.+/`)
		curUrl = res.FindString(url)
		imageUrl = curUrl + pathImg
	}
	fmt.Println(imageUrl)
	resp, err := http.Get(imageUrl)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	ct := resp.Header.Get("Content-Type")
	enc := encode(body)
	out := format(enc, ct)
	base64Encoded[i] = template.URL(out)

	//this mutex counts the number of pictures (need for multithreading)
	mutex.Lock()
	total++
	mutex.Unlock()
	runtime.Gosched()
}

// Helper function to pull the src attribute from a Token

/*func getSrc(t html.Token) (ok bool, src string) {
    // Iterate over all of the Token's attributes until we find an "href"
    for _, a := range t.Attr {
        if a.Key == "src" {
            src = a.Val
            ok = true
        }
    }
    return
}
*/

func handler(w http.ResponseWriter, r *http.Request) {
	total = 0
	base64Encoded = map[int]template.URL{}

	urlparams, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		log.Fatal(err)
	}

	response, err := http.Get(urlparams["url"][0])
	if err != nil {
		log.Fatal(err)
	}
	bytes_, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	var htmlStr string = string(bytes_)

	//save <img>
	re := regexp.MustCompile(`<img[^>]+?>`)
	var img []string = re.FindAllString(htmlStr, -1)

	//take attr src from tag <img>
	var src string = ""
	reg := regexp.MustCompile(`<img.*src="([^ ]+?)".*>`)
	i := 0
	for _, value := range img {
		src = reg.ReplaceAllString(value, "$1")
		//fmt.Println(src)
		go saveImg(w, src, urlparams["url"][0], i) //call func to handle image
		i++
	}
	response.Body.Close()

	//I tried to use html.NewTokinizer, but it doen not find all img tags
	/*z := html.NewTokenizer(b)
	  i := 0
	  loop:
	  for  {
	      tt := z.Next()

	      switch {
	      case tt == html.ErrorToken:
	          // End of the html
	          break loop

	      case tt == html.StartTagToken:
	          t := z.Token()

	          // Check if the token is an <img> tag
	          isAnchor := t.Data == "img"
	          if !isAnchor {
	              continue
	          }

	          // Extract the src value, if there is one
	          ok, src := getSrc(t)
	          if !ok {
	              continue
	          }


	        //  foundSrcs = append(foundSrcs, src)
	        go saveImg(w, src, url, i)
	          i++
	      }
	  }*/
	fmt.Println("before loop")
	fmt.Println(i)
	//this cycle is for waiting when all the pictures would be loaded
	for {
		if total == i {
			break
		}
	}

	var htmlTemplate = `
   <html>
        <body>
            {{range $element := .}}
                <img src={{$element}}>
            {{end}}
        </html>
    </body>`

	t := template.New("t")
	t_, err := t.Parse(htmlTemplate)
	if err != nil {
		log.Fatal(err)
	}

	err = t_.Execute(w, base64Encoded)
	if err != nil {
		log.Fatal(err)
	}

}

//this handler needs to prevent a second request to page
func handlerICon(w http.ResponseWriter, r *http.Request) {}

func main() {

	http.HandleFunc("/favicon.ico", handlerICon)
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8888", nil)

}
